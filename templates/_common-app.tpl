{{- define "app.deployment" -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .name }}
  namespace: {{ .namespace }}
  labels:
    app: {{ .name }}
spec:
  replicas: {{ .replicas }}
  selector:
    matchLabels:
      app: {{ .name }}
  template:
    metadata:
      labels:
        app: {{ .name }}
    spec:
      containers:
        - name: {{ .name }}
          image: {{ .container.image }}:{{ .container.imageVersion }}
          imagePullPolicy: {{ .container.imagePullPolicy }}
          ports:
            - containerPort: {{ .container.port }}
              protocol: TCP
          resources:
            requests:
                memory: {{ .container.resources.request.memory }}
                cpu: {{ .container.resources.request.cpu }}
            limits:
                memory: {{ .container.resources.limits.memory }}
                cpu: {{ .container.resources.limits.cpu }}
{{- if .container.envFrom }}
          envFrom:
{{ toYaml .container.envFrom | indent 10 }}
{{- end}}
      imagePullSecrets:
              - name: {{ .imagePullSecrets.name }}
{{- end}}


{{- define "app.service" -}}
apiVersion: v1
kind: Service
metadata:
  name: {{ .name }}
  namespace: {{ .namespace }}
spec:
  selector:
    app: {{ .name }}
  {{- if .service.type }}type: {{ .service.type }}{{ end }}
  ports:
    - port: {{ .service.port }}
      targetPort: {{ .service.targetPort }}
      {{- if .service.nodePort }}nodePort: {{ .service.nodePort }}{{ end }}
{{- end}}